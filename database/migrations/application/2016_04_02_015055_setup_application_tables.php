<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetupApplicationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('documents', function ($table) {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('description');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at');

        });

        Schema::create('applications', function ($table) {
            $table->increments('id')->unsigned();
            $table->integer('document')->unsigned();

            $table->string('applicant_name');
            $table->string('applicant_address');
            $table->string('applicant_phone');
            $table->string('date_received');
            $table->string('date_to_appear');
            $table->string('date_appeared');
            $table->string('date_sent_record_room');
            $table->string('date_received_from_record_room');
            $table->string('date_notice_for_correction');
            $table->string('date_notice_for_fund');
            $table->string('date_notice_compiled');
            $table->string('date_copy_read');
            $table->string('date_copy_dispatched');
            
            $table->string('amt_received');
            $table->string('amt_court_fee');
            $table->string('amt_commission');
            $table->string('amt_postage');
            $table->string('amt_refunded');
            $table->string('amt_advance');
            
            $table->enum('status', array('pending', 'processing','complete'));	
            $table->string('remark');
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at');
            /**
             * Add Foreign/Unique/Index
             */
            $table->foreign('document')
                ->references('id')
                ->on(config('access.document_table'))
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        /**
         * Drop tables
         */
        Schema::drop('applications');
        Schema::drop('documents');
    }
}
