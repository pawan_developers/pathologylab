@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.access.documents.management'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.documents.management') }}
        <small></small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>

            <div class="box-tools pull-right">
                @include('backend.access.includes.partials.header-buttons')
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>{{ trans('labels.backend.access.documents.table.id') }}</th>
                        <th>{{ trans('labels.backend.access.documents.table.name') }}</th>
                        <th>{{ trans('labels.backend.access.documents.table.description') }}</th>
                        <th class="visible-lg">{{ trans('labels.backend.access.documents.table.created') }}</th>
                        <th class="visible-lg">{{ trans('labels.backend.access.documents.table.last_updated') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($documents as $document)
                            <tr>
                                <td>{!! $document->id !!}</td>
                                <td>{!! $document->name !!}</td>
                                <td>{!! $document->description !!}</td>
                                <td class="visible-lg">{!! $document->created_at->diffForHumans() !!}</td>
                                <td class="visible-lg">{!! $document->updated_at->diffForHumans() !!}</td>
                                <td>{!! $document->action_buttons !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="pull-left">
                {!! $documents->total() !!} {{ trans_choice('labels.backend.access.documents.table.total', $documents->total()) }}
            </div>

            <div class="pull-right">
                {!! $documents->render() !!}
            </div>

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
@stop
