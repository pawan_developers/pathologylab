<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <a href="https://www.upwork.com/freelancers/~01499100b6be2a7938" target="_blank">{{ trans('strings.backend.general.developer_link') }}</a>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; {!! date('Y') !!} <a href="#">{!! app_name() !!}</a>.</strong> {{ trans('strings.backend.general.all_rights_reserved') }}
</footer>
