<?php

namespace App\Models\Access\Document\Traits\Relationship;

/**
 * Class TestRelationship
 * @package App\Models\Access\Document\Traits\Relationship
 */
trait DocumentRelationship
{
    /**
     * @return mixed
     */
    public function documents()
    {
        return $this->belongsToMany(config('access.application'), config('access.document'), 'document_id', 'id');
    }
}
