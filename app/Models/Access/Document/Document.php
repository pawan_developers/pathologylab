<?php

namespace App\Models\Access\Document;

use Illuminate\Database\Eloquent\Model;
//use App\Models\Access\Document\Traits\DocumentAccess;
use App\Models\Access\Document\Traits\Attribute\DocumentAttribute;
//use App\Models\Access\Document\Document\Relationship\DocumentRelationship;

/**
 * Class Document
 * @package App\Models\Access\Document
 */
class Document extends Model
{
    //use DocumentAccess, DocumentAttribute, DocumentRelationship;
    use DocumentAttribute;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = 'documents';
    }
}
