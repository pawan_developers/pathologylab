<?php

namespace App\Repositories\Backend\Access\Document;

use App\Models\Access\Document\Document;
use App\Exceptions\GeneralException;

/**
 * Class EloquentDocumentRepository
 * @package App\Repositories\Backend\Access\Document
 */
class EloquentDocumentRepository implements DocumentRepositoryContract
{
    /**
     * @param  $id
     * @throws GeneralException
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|\Illuminate\Support\Collection|null|static
     */
    public function findOrThrowException($id)
    {
        if (! is_null(Document::find($id))) {
                return Document::find($id);
        }

        throw new GeneralException(trans('exceptions.backend.access.documents.not_found'));
    }

    /**
     * @param  $per_page
     * @param  string      $order_by
     * @param  string      $sort
     * @return mixed
     */
    public function getDocumentsPaginated($per_page, $order_by = 'name', $sort = 'asc')
    {
        return Document::orderBy($order_by, $sort)->paginate($per_page);
    }

    /**
     * @param  string  $order_by
     * @param  string  $sort
     * @param  bool    $withPermissions
     * @return mixed
     */
    public function getAllDocuments($order_by = 'name', $sort = 'asc')
    {
        return Document::orderBy($order_by, $sort)->get();
    }

    /**
     * @param  $input
     * @throws GeneralException
     * @return bool
     */
    public function create($input)
    {
        $document = $this->createDocumentStub($input);

        if ($document->save()) {
             return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.documents.create_error'));
    }

    /**
     * @param  $id
     * @param  $input
     * @throws GeneralException
     * @return bool
     */
    public function update($id, $input)
    {
        $document = $this->findOrThrowException($id);

        
        $document->name = $input['name'];
        $document->sort = isset($input['sort']) && strlen($input['sort']) > 0 && is_numeric($input['sort']) ? (int) $input['sort'] : 0;

        if ($document->save()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.documents.update_error'));
    }

    /**
     * @param  $id
     * @throws GeneralException
     * @return bool
     */
    public function destroy($id)
    {

        $document = $this->findOrThrowException($id, true);

        if ($document->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.documents.delete_error'));
    }
	
    /**
     * @param  $id
     * @param  $status
     * @throws GeneralException
     * @return bool
     */
    public function mark($id, $status)
    {
        $document         = $this->findOrThrowException($id);
        $document->status = $status;

        if ($document->save()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.document.mark_error'));
    }
        /**
     * @param  $input
     * @return mixed
     */
    private function createDocumentStub($input)
    {
        $document               		= new Document;
        $document->name        			= $input['name'];
        $document->description  		= $input['description'];
        //$document->status       		= isset($input['status']) ? $input['status'] : 1;
        return $document;
    }

}
