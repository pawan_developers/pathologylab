<?php

namespace App\Http\Controllers\Backend\Access\Document;

use App\Http\Controllers\Controller;

use App\Repositories\Backend\Access\Document\DocumentRepositoryContract;

use App\Http\Requests\Backend\Access\Document\EditDocumentRequest;
use App\Http\Requests\Backend\Access\Document\StoreDocumentRequest;
use App\Http\Requests\Backend\Access\Document\CreateDocumentRequest;
use App\Http\Requests\Backend\Access\Document\DeleteDocumentRequest;
use App\Http\Requests\Backend\Access\Document\UpdateDocumentRequest;

/**
 * Class DocumentController
 * @package App\Http\Controllers\Backend\Document
 */
class DocumentController extends Controller
{
    /**
     * @var DocumentRepositoryContract
     */
    protected $documents;

    /**
     * @param DocumentRepositoryContract       $documents
     */
    public function __construct( DocumentRepositoryContract $documents )
    {
        $this->documents       = $documents;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return view('backend.access.documents.index')
            ->withDocuments($this->documents->getDocumentsPaginated(50));
    }

    /**
     * @param  CreateDocumentRequest                 $request
     * @return mixed
     */
    public function create(CreateDocumentRequest $request)
    {
        return view('backend.access.documents.create');
    }

    /**
     * @param  StoreDocumentRequest $request
     * @return mixed
     */
    public function store(StoreDocumentRequest $request)
    {
        $this->documents->create($request->all());
        return redirect()->route('admin.access.documents.index')->withFlashSuccess(trans('alerts.backend.documents.created'));
    }

    /**
     * @param  $id
     * @param  EditDocumentRequest                   $request
     * @return mixed
     */
    public function edit($id)
    {
        $document = $this->documents->findOrThrowException($id, true);
        return view('backend.access.documents.edit')
            ->withDocument($document);
    }
	    /**
     * @param  $id
     * @param  $status
     * @return mixed
     */
    public function mark($id, $status)
    {
        $this->documents->mark($id, $status);
        return redirect()->back()->withFlashSuccess(trans('alerts.backend.documents.updated'));
    }
    /**
     * @param  $id
     * @param  UpdateDocumentRequest $request
     * @return mixed
     */
    public function update($id, UpdateDocumentRequest $request)
    {
        $this->documents->update($id, $request->all());
        return redirect()->route('admin.access.documents.index')->withFlashSuccess(trans('alerts.backend.documents.updated'));
    }

    /**
     * @param  $id
     * @param  DeleteDocumentRequest $request
     * @return mixed
     */
    public function destroy($id, DeleteDocumentRequest $request)
    {
        $this->documents->destroy($id);
        return redirect()->route('admin.access.documents.index')->withFlashSuccess(trans('alerts.backend.documents.deleted'));
    }
}
