<?php

namespace App\Http\Requests\Backend\Access\Document;

use App\Http\Requests\Request;

/**
 * Class EditDocumentRequest
 * @package App\Http\Requests\Backend\Access\Document
 */
class EditDocumentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('edit-documents');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
