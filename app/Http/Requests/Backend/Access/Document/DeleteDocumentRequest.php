<?php

namespace App\Http\Requests\Backend\Access\Document;

use App\Http\Requests\Request;

/**
 * Class DeleteDocumentRequest
 * @package App\Http\Requests\Backend\Access\Document
 */
class DeleteDocumentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return access()->allow('delete-documents');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
