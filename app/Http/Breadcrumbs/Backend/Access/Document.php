<?php

Breadcrumbs::register('admin.access.documents.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.access.documents.management'), route('admin.access.documents.index'));
});

Breadcrumbs::register('admin.access.documents.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.access.documents.index');
    $breadcrumbs->push(trans('menus.backend.access.documents.create'), route('admin.access.documents.create'));
});

Breadcrumbs::register('admin.access.documents.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.access.documents.index');
    $breadcrumbs->push(trans('menus.backend.access.documents.edit'), route('admin.access.documents.edit', $id));
});
